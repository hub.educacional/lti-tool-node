'use strict';

const express = require('express');
var bodyParser = require('body-parser');

const { launch } = require('./ltiTool');

// Setup Express App
const PORT = 5000;
var app = express();
app.use(bodyParser.urlencoded({ extended: true })); //Add support to x-www-form-urlencoded

// Setup POST /launch endpoint
app.post('/launch', launch);

app.listen(PORT, () => console.log(`Example app listening on port ${PORT}!`));
