'use-strict'

const lti = require('ims-lti');

// Hub Educacional OAuth1 client
let hub_client = {
  client_id: 'hub.educacional.com',
  client_secret: 'secret'
};

// Client Object
// Emulates a client database
// In real application this is likely to be an RDBMS or a NoSQL
let EmulatedClientDB = {};

// Add Hub Client to Emulated Client Database
EmulatedClientDB[hub_client.client_id] = hub_client.client_secret;

// Basic Launch Message LTI 1.0
// Request Handler
function LTI1p0BasicLaunch (req, res) {
  console.log('Message Paramenters:', req.body);

  // Search client in Client Database by client_id (oauth_consumer_key)
  let client_id = req.body["oauth_consumer_key"];
  if (client_id) {
    let client_secret = EmulatedClientDB[client_id];

    // After finding client by client_id
    // lti.Provider instance is created with client OAuth1 client data
    const provider = new lti.Provider(client_id, client_secret);

    // lti.Provider.valid_request() validates both oauth1 signature
    // and LTI basic launch
    provider.valid_request(req, (err, is_valid) => {
      let response;
      if (err) {
        console.log('error:', err);
        response = {
          ...err,
          body: req.body
        }
        res.send(response, 401);
      }
      if (is_valid) {
        console.log('is valid:', is_valid);
        response = {
          message: 'Passed!',
          body: req.body
        }
        res.send(response);
      }
      });
  } else {
    console.log('error: Missing oauth_consumer_key parameter!');
    let response;
    response = {
      message: 'Missing oauth_consumer_key parameter!',
      body: req.body
    }
    res.status(401)
       .send(response)
  }
}

// Launch Function Definition
module.exports.launch = LTI1p0BasicLaunch;