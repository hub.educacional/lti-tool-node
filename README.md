# Hub Educacional - Exemplo de Ferramenta LTI (Node)

## Implementação (Provedor de Ferramenta LTI)

A implementação desse projeto deve como diretrizes os seguintes documentos: [Guia de implementação LTI](https://www.imsglobal.org/specs/ltiv1p1p1/implementation-guide), [Melhores práticas LTI](http://ltiapps.net/guide/LTI_Best_Practice.pdf) e o auxílio de [Inicialização Básica LTI: Parâmetros do POST](https://www.eduappcenter.com/docs/basics/post_parameters). Use esses documentos como referência para sua aplicação.

Esse exemplo foi escrito em Node + [Express](https://github.com/expressjs/express) para prover o endpoint de inicialização LTI (`POST /launch`) utilizando a biblioteca [ims-lti](https://github.com/omsmith/ims-lti) para verificação LTI e OAuth1. Abaixo seguem os tópicos que serão abordados:

* Implementação (Provedor de Ferramenta LTI)
* Verificação OAuth1 e validação da mensagem de inicialização LTI 1.0
* Instalação
* Executando o app (Provedor de Ferramenta LTI)
* Biblioteca ims-lti

### Estrutura do projeto

Essa é a árvore de arquivos desse projeto:
```
.
├── app.js
├── ltiTool.js
├── package.json
├── package-lock.json
└── README.md
```
O arquivo `app.js` é o código da nossa aplicação Express, aqui está um código bem simples que define o endpoint de inicialização LTI (`POST /launch`) para a função `LTI1p0BasicLaunch()` do arquivo `ltiTool.js` que trata da verificação da mensagem básica LTI de inicialização. A função `LTI1p0BasicLaunch()` utiliza a biblioteca `ims-lti` para realizar a verificação OAuth1 e as validações da mensagem LTI. No arquivo `ltiTool.js` está definido também o o objeto `EmulatedClientDB = {}` que irá emular uma base de dados de clientes OAuth1. **Note** que em uma aplicação real é muito provável que essa base de clientes seja um RDBMS ou um banco de dados NoSQL. No próximo item será explicado o processo e verificação OAuth1 e LTI.


### Verificação OAuth1 e validação da mensagem de inicialização LTI 1.0

No arquivo `ltiTool.js` você verifica a definição do endpoint e da função `LTI1p0BasicLaunch()`. Essa função implementa to tratamento da verificação e validação da mensagem de inicialização LTI 1.0 feita pela biblioteca `ims-lti`.

O código abaixo mostra uma implementação simplificada, porém funcional, da validação e verificação da mensagem LTI recebida (A seguir será explicado o passo a passo das partes mais importantes dessa função):

```javascript
// Mensagem Básica de Inicialização LTI 1.0
// Tratamento da Requisição
function LTI1p0BasicLaunch (req, res) {
  // Recupera client_secret pelo id do cliente(oauth_consumer_key)
  let client_id = req.body["oauth_consumer_key"];
  if (client_id) {
    let client_secret = EmulatedClientDB[client_id];

    // Após encontrar os dados do cliente
    // É instanciado um objeto lti.Provider com os dados do cliente
    const provider = new lti.Provider(client_id, client_secret);

    // lti.Provider.valid_request() realiza a validação tanto da assinatura OAuth1
    // quanto dos parâmetros LTI da mensagem
    provider.valid_request(req, (err, is_valid) => {
      let response;
      // Caso a validação falhe, retorna erro com o motivo.
      if (err) {
        console.log('error:', err);
        response = {
          ...err,
          body: req.body
        }
        res.send(response, 401);
      }
      // Caso a mensagem seja válida, retorna sucesso para o cliente.
      if (is_valid) {
        console.log('is valid:', is_valid);
        response = {
          message: 'Passou! Mostrar recurso privado!',
          body: req.body
        }
        res.send(response);
      }
      });
  }
  // Código modificado para simplificação
}
```

Em resumo, a função `LTI1p0BasicLaunch()` realiza os seguintes passos:
```
1. Recupera client_secret a partir do id do cliente.
2. Instancia um objeto lti.Provider com os dados do cliente (client_id, client_secret).
3. Valida a mensagem com provider.valid_request. Se válido (OAuth1 Ok e Mensagem LTI Ok),
 a. Prepara o recurso autenticado e retorna para o usuário/Retorna Sucesso.
 b. Caso contrário, não exibir recurso privado para o requisitante/Retorna Falha.
```

Neste código simplificado, caso a mensagem LTI seja verificada, o recurso mostrado é apenas `Passou! Mostrar recurso privado!` com código `200`, ou uma mensagem de erro com código `401` caso contrário. No código implementado algumas informações extras são apresentadas, como os parâmetros lidos e mensagens de erros mais explicativas para facilitar testes, mas seguem o mesmo padrão apresentado aqui.

**IMPORTANTE** Fique ciente, entretanto, que a forma de apresentação dos recursos privados varia de aplicação para aplicação e que esse é somente um exemplo. É muito provável que sua aplicação tenha sua própria gestão de usuários e autenticação, neste caso você deve implementar todas as etapas necessárias para a exibição final do recurso para o aluno ou usuário, como levantamento de sessão do usuário, alocação de recursos, redirecionamentos de páginas necessários, etc.

## Instalação
Antes de testar instale os requisitos:
```
$ npm install
```

## Executando o app (Provedor de Ferramenta LTI)

Na raiz do projeto:
```
$ npm start
```
ou,
```
$ node app
```

## Biblioteca ims-lti

LTI 1.0 é construído sobre o padrão de segurança OAuth1 para autorização e autenticação. Nesse exemplo é utilizada a biblioteca [ims-lti](https://github.com/omsmith/ims-lti) que já implementa uma verificação de assinatura OAuth1, além da validação LTI. Caso sua aplicação já tenha sido construída sobre OAuth1, analise se pode reaproveitar a base de dados para clientes OAuth e caso queira realizar somente as validações LTI, pode começar verificando a implementação da biblioteca. Caso sua aplicação seja ou será escrita em outro framework, apenas adapte o provider.valid_request para seu produto.